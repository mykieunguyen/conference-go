from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)

class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        if isinstance(o, datetime):
        #    return o.isoformat()
            return o.isoformat()
        # otherwise
        else:
        #    return super().default(o)
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            content_dictionary = {}
            if hasattr(o, "get_api_url"):
                content_dictionary["href"] = o.get_api_url()

            for item in self.properties:
                value = getattr(o, item)
                if item in self.encoders:
                    encoder = self.encoders[item]
                    value = encoder.default(value)
                content_dictionary[item] = value
            content_dictionary.update(self.get_extra_data(o))
            return content_dictionary
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
