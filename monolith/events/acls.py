from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_location_photo_url(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    query = {
        "query": f"{city} {state}"
    }
    url = 'https://api.pexels.com/v1/search'
    response = requests.get(url, params=query, headers=headers)
    picture_url = {
        'picture_url': response.json()["photos"][0]["src"]["original"]
    }

    return picture_url

def get_weather_data(city, state):
    # Geocode API: Get lat lon
    query = {
        'q':f'{city}, {state}, USA',
        "appid":OPEN_WEATHER_API_KEY,
        }
    url = 'http://api.openweathermap.org/geo/1.0/direct'
    lat_long = requests.get(url, params=query)
    lat = lat_long.json()[0]['lat']
    lon = lat_long.json()[0]['lon']
    # Weather data
    query = {
        'lat': lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": 'imperial',
    }
    url = 'https://api.openweathermap.org/data/2.5/weather'
    response = requests.get(url, params=query)

    temp = response.json()["main"]["temp"]
    description = response.json()["weather"][0]['description']

    return {
        "temp": temp,
        "description": description,
    }
